﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.Services
{
    public interface ICourseService
    {
        IEnumerable<Courses> GetAll();
        Task<Courses> GetCourse(int id);
        Task AddCourse(Courses course);
        Task<bool> UpdateCourse(Courses course);
        Task<bool> DelCourse(int id);
    }
}
