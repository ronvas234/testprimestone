﻿using Common.DTOs;
using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.Services
{
   public interface ISecurityService
    {
        Task<Users> GetLoginByCredentials(UsersLoginRequest login);
    }
}
