﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.Services
{
    public interface IStudentService
    {
        IEnumerable<Students> GetAll();
        Task<Students> GetStudent(int id);
        Task AddStudent(Students student);
        Task<bool> UpdateStudent(Students student);
        Task<bool> DelStudent(int id);
    }
}
