﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.Services
{
    public interface IStudentCourseService
    {
        IEnumerable<StudentsCourses> GetAll();
        Task<StudentsCourses> GetStudentCourse(int id);
        Task AddStudentCourse(StudentsCourses studentCourse);
        Task<bool> UpdateStudentCourse(StudentsCourses studentCourse);
        Task<bool> DelStudentCourse(int id);
    }
}
