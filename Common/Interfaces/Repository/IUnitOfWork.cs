﻿using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Students> StudentRepository { get; }
        IRepository<Courses> CourseRepository { get; }
        IRepository<StudentsCourses> StudentCourseRepository { get; }
        ISecurityRepository SecurityRepository { get; }
        void SaveChanges();
        Task SaveChangesAsync();
    }
}
