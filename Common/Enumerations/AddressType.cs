﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Enumerations
{
    public enum AddressType
    {
        Primary = 0,
        Work= 1,
        Temporary = 2,
    }
}
