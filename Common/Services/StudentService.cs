﻿using Common.Entities;
using Common.Interfaces.Repository;
using Common.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.Services
{
    public class StudentService : IStudentService
    {
        private readonly IUnitOfWork _unitOfWork;
        public StudentService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public IEnumerable<Students> GetAll()
        {
            return _unitOfWork.StudentRepository.GetAll();
        }

        public async Task<Students> GetStudent(int id)
        {
            return await _unitOfWork.StudentRepository.GetById(id);
        }

        public async Task AddStudent(Students student)
        {
            await _unitOfWork.StudentRepository.Add(student);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> UpdateStudent(Students student)
        {
            _unitOfWork.StudentRepository.Update(student);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DelStudent(int id)
        {
            await _unitOfWork.StudentRepository.Delete(id);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

    }
}
