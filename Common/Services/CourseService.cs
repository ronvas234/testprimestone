﻿using Common.Entities;
using Common.Interfaces.Repository;
using Common.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.Services
{
    public class CourseService : ICourseService
    {
        private readonly IUnitOfWork _unitOfWork;
        public CourseService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Courses> GetAll()
        {
            return _unitOfWork.CourseRepository.GetAll();
        }

        public async Task<Courses> GetCourse(int id)
        {
            return await _unitOfWork.CourseRepository.GetById(id);
        }
        public async Task AddCourse(Courses course)
        {
            await _unitOfWork.CourseRepository.Add(course);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> DelCourse(int id)
        {
            await _unitOfWork.CourseRepository.Delete(id);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async Task<bool> UpdateCourse(Courses course)
        {
            _unitOfWork.CourseRepository.Update(course);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }
    }
}
