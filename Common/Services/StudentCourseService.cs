﻿using Common.Entities;
using Common.Interfaces.Repository;
using Common.Interfaces.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Common.Services
{
    public class StudentCourseService : IStudentCourseService
    {
        private readonly IUnitOfWork _unitOfWork;
        public StudentCourseService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<StudentsCourses> GetAll()
        {
            return _unitOfWork.StudentCourseRepository.GetAll();
        }

        public async Task<StudentsCourses> GetStudentCourse(int id)
        {
            return await _unitOfWork.StudentCourseRepository.GetById(id);
        }

        public async Task AddStudentCourse(StudentsCourses studentCourse)
        {
            await _unitOfWork.StudentCourseRepository.Add(studentCourse);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<bool> UpdateStudentCourse(StudentsCourses studentCourse)
        {
            _unitOfWork.StudentCourseRepository.Update(studentCourse);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DelStudentCourse(int id)
        {
            await _unitOfWork.StudentCourseRepository.Delete(id);
            await _unitOfWork.SaveChangesAsync();
            return true;
        }
    }
}
