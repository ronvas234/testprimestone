﻿using Common.DTOs;
using Common.Entities;
using Common.Exceptions;
using Common.Interfaces.Repository;
using Common.Interfaces.Services;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Common.Services
{
    public class SecurityService : ISecurityService
    {
        private readonly IUnitOfWork _unitOfWork;
        public SecurityService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<Users> GetLoginByCredentials(UsersLoginRequest login)
        {
            var users = await _unitOfWork.SecurityRepository.GetLoginByCredentials(login);
            if (users == null)
            {
                throw new NotFoundException("El usuario no existe por favor verificar");
            }
            return users;
        }
    }
}
