﻿using Common.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class Students:BaseEntity
    {
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public Genders Genders { get; set; }
        [ForeignKey("StudentId")]
        public ICollection<StudentsAddress> StudentsAddress { get; set; }
        [ForeignKey("StudentId")]
        public ICollection<StudentsCourses> StudentsCourses { get; set; }
    }
}
