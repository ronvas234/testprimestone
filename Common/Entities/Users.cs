﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
   public class Users : BaseEntity
    {
        [StringLength(50)]
        public string Email { get; set; }
        [StringLength(450)]
        public string Password { get; set; }
        public int StudentId { get; set; }
    }
}
