﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class Courses: BaseEntity
    {
        [StringLength(10)]
        public string CodeCourse { get; set; }
        [StringLength(50)]
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        [ForeignKey("CourseId")]
        public ICollection<StudentsCourses> StudentsCourses { get; set; }
    }
}
