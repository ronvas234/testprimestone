﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class StudentsCourses:BaseEntity
    {
        public int StudentId { get; set; }
        public int CourseId { get; set; }
        public DateTime RegistrationDate { get; set; }
        public Courses Courses { get; set; }
        public Students Students { get; set; }
    }
}
