﻿using Common.Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class StudentsAddress:BaseEntity
    {
        public int StudentId { get; set; }
        public AddressType AddressType { get; set; }
        [StringLength(200)]
        public string Address { get; set; }
        [StringLength(30)]
        public string State { get; set; }
        [StringLength(30)]
        public string City { get; set; }
        [StringLength(50)]
        public string Country { get; set; }
        public Students Students { get; set; }
    }
}
