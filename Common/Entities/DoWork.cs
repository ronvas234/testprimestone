﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entities
{
    public class DoWork:BaseEntity
    {
        public bool IsErased { get; set; }
        [StringLength(300)]
        public string Events { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
