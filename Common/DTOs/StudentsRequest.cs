﻿using Common.Enumerations;
using Common.Helpers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTOs
{
    public class StudentsRequest
    {
        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }
        [Required]
        [StringLength(50)]
        public string LastName { get; set; }
        [Required]
        public DateTime BirthDate { get; set; }
        [Required]
        public Genders Genders { get; set; }
        [ModelBinder(BinderType = typeof(TypeBinder<List<StudentsAddressRequest>>))]
        public virtual List<StudentsAddressRequest> StudentsAddressLine { get; set; }
    }
}
