﻿using Common.Enumerations;
using Common.Helpers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTOs
{
   public class StudentsResponse
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDate { get; set; }
        public Genders Genders { get; set; }
        public ICollection<StudentsAddressResponse> StudentsAddressLine { get; set; }
    }
}
