﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DTOs
{
    public class UsersLoginRequest
    {
        [Required(ErrorMessage = "El campo correo electrónico es obligatorio.")]
        [EmailAddress(ErrorMessage = "Debe ingresar un email correcto")]
        public string Email { get; set; }
        [Required(ErrorMessage = "La contraseña es obligatorio.")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "El número de caracteres del {0} debe ser al menos {2}", MinimumLength = 8)]
        public string Password { get; set; }
    }
}
