﻿using Common.Entities;
using Infraestructure.Data;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infraestructure.JobServices
{
    public class JobRecurrentService : IHostedService,IDisposable
    {
        public readonly IServiceProvider _service;
        private Timer _timer;
        public JobRecurrentService(IServiceProvider service)
        {
            _service = service;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero, TimeSpan.FromSeconds(10));
            return Task.CompletedTask;
        }
        private void DoWork(object obj)
        {
            using (var scope = _service.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                var entity = new DoWork();
                entity.CreatedDate = DateTime.Now;
                entity.Events = "Job execute at " + DateTime.Now.ToString();
                entity.IsErased = false;
                context.DoWork.Add(entity);
                context.SaveChanges();
            }
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
