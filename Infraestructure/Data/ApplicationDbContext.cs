﻿using Common.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }
        public DbSet<Courses> Courses { get; set; }
        public DbSet<Students> Students { get; set; }
        public DbSet<StudentsAddress> StudentsAddress { get; set; }
        public DbSet<StudentsCourses> StudentsCourses { get; set; }
        public DbSet<DoWork> DoWork { get; set; }

    }
}
