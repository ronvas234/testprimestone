﻿using Common.DTOs;
using Common.Entities;
using Common.Interfaces.Repository;
using Infraestructure.Data;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Repositories
{
    public class SecurityRepository : BaseRepository<Users>, ISecurityRepository
    {
        public readonly ApplicationDbContext _context;
        public SecurityRepository(ApplicationDbContext context) : base(context)
        {
            _context = context;
        }

        public async Task<Users> GetLoginByCredentials(UsersLoginRequest login)
        {
            return await _entities.FirstOrDefaultAsync(x => x.Email == login.Email);
        }
    }
}
