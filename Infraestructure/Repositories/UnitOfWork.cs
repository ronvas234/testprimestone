﻿using Common.Entities;
using Common.Interfaces.Repository;
using Infraestructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;
        private readonly IRepository<Students> _studentRepository;
        private readonly IRepository<Courses> _courseRepository;
        private readonly IRepository<StudentsCourses> _studentCourseRepository;
        private readonly ISecurityRepository _securityRepository;

        public UnitOfWork(ApplicationDbContext contex)
        {
            _context = contex;
        }

        public IRepository<Students> StudentRepository => _studentRepository ?? new BaseRepository<Students>(_context);
        public IRepository<Courses> CourseRepository => _courseRepository ?? new BaseRepository<Courses>(_context);
        public IRepository<StudentsCourses> StudentCourseRepository => _studentCourseRepository ?? new BaseRepository<StudentsCourses>(_context);
        public ISecurityRepository SecurityRepository => _securityRepository ?? new SecurityRepository(_context);
        public void Dispose()
        {
            if (_context != null)
            {
                _context.Dispose();
            }
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
