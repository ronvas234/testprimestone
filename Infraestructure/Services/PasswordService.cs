﻿using Common.Interfaces.Services;
using Infraestructure.Options;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Services
{
    public class PasswordService : IPasswordService
    {
        private readonly PasswordOptions _passwordService;
        public PasswordService(IOptions<PasswordOptions> appSetting)
        {
            _passwordService = appSetting.Value;
        }

        public bool Check(string hash, string password)
        {
            var parts = hash.Split('.');
            if (parts.Length != 3)
            {
                throw new FormatException("Unexpected hash format");
            }

            var iterations = Convert.ToInt32(parts[0]);
            var salt = Convert.FromBase64String(parts[1]);
            var key = Convert.FromBase64String(parts[2]);

            using (var algorithm = new Rfc2898DeriveBytes(
                password,
                salt,
                iterations
                ))
            {
                var keyToCheck = algorithm.GetBytes(_passwordService.KeySize);
                return keyToCheck.SequenceEqual(key);
            }
        }

        public string Hash(string password)
        {
            //PBKDF2 implementation
            using (var algorithm = new Rfc2898DeriveBytes(
                password,
                _passwordService.SaltSize,
                _passwordService.Iterations
                ))
            {
                var key = Convert.ToBase64String(algorithm.GetBytes(_passwordService.KeySize));
                var salt = Convert.ToBase64String(algorithm.Salt);

                return $"{_passwordService.Iterations}.{salt}.{key}";
            }
        }
    }
}
