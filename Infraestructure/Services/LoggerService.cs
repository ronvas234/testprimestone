﻿using Common.Interfaces.Services;
using Nancy.Json;
using Microsoft.Extensions.Logging;

namespace Infraestructure.Services
{
    public class LoggerService : ILoggerService
    {
        private readonly ILogger<LoggerService> _logger;
        public LoggerService(ILogger<LoggerService> logger)
        {
            _logger = logger;
        }

        public void LogInformation(string message, params object[] agurments)
        {
            if (agurments != null)
            {
                var obj = new object[agurments.Length];
                for (var i = 0; i < agurments.Length; i++)
                {
                    obj[i] = new JavaScriptSerializer().Serialize(agurments[i]);
                }
                _logger.LogInformation(message, obj);
            }
        }

        public void LogError(string message, params object[] agurments)
        {
            if (agurments != null)
            {
                var obj = new object[agurments.Length];
                for (var i = 0; i < agurments.Length; i++)
                {
                    obj[i] = new JavaScriptSerializer().Serialize(agurments[i]);
                }
                _logger.LogError(message, obj);
            }
        }

        public void LogWarning(string message, params object[] agurments)
        {
            if (agurments != null)
            {
                var obj = new object[agurments.Length];
                for (var i = 0; i < agurments.Length; i++)
                {
                    obj[i] = new JavaScriptSerializer().Serialize(agurments[i]);
                }
                _logger.LogWarning(message, obj);
            }
        }
    }
}
