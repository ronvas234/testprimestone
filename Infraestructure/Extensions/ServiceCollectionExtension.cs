﻿using Common.CustomEntities;
using Common.Interfaces.Repository;
using Common.Interfaces.Services;
using Common.Services;
using Infraestructure.Data;
using Infraestructure.JobServices;
using Infraestructure.Options;
using Infraestructure.Repositories;
using Infraestructure.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace Infraestructure.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static IServiceCollection AddDbContexts(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(
                options => options.UseSqlServer(
                    configuration.GetConnectionString("DefaultConnection")
                    )
            );

            return services;
        }
        public static IServiceCollection AddOptions(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<PaginationOptions>(options => configuration.GetSection("Pagination").Bind(options));
            services.Configure<PasswordOptions>(options => configuration.GetSection("PasswordOptions").Bind(options));
            services.Configure<AuthenticationOptions>(options => configuration.GetSection("Authentication").Bind(options));
            return services;
        }
        public static IServiceCollection AddCorsPolicy(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddCors(options => {
                options.AddPolicy(name: "Cors",
                    builder => {
                        builder.WithOrigins(configuration["WebBase"])
                        .WithHeaders("*")
                        .WithMethods("PUT", "DELETE", "GET", "POST");
                    });
            });
            return services;
        }

        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<IStudentService, StudentService>();
            services.AddTransient<ICourseService, CourseService>();
            services.AddTransient<IStudentCourseService, StudentCourseService>();
            services.AddTransient<ISecurityService, SecurityService>();
            services.AddTransient<IHostedService, JobRecurrentService>();

            services.AddScoped(typeof(IRepository<>), typeof(BaseRepository<>));
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IPasswordService, PasswordService>();
            services.AddScoped<ILoggerService, LoggerService>();


            return services;
        }

        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(doc =>
            {
                doc.SwaggerDoc("v1", new OpenApiInfo { Title = "Students", Version = "v1" });
            });

            return services;
        }
    }
}
