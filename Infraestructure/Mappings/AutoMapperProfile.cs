﻿using AutoMapper;
using Common.DTOs;
using Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infraestructure.Mappings
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Students, StudentsRequest>().ReverseMap();
            CreateMap<StudentsResponse, Students>().ReverseMap();
            CreateMap<Courses, CoursesRequest>().ReverseMap();
            CreateMap<CoursesResponse, Courses>().ReverseMap();
            CreateMap<StudentsAddress, StudentsAddressRequest>().ReverseMap();
            CreateMap<StudentsAddressResponse, Students>().ReverseMap();
            CreateMap<Students, StudentsRequest>().ReverseMap();
            CreateMap<StudentsResponse, Students>().ReverseMap();
        }
    }
}
