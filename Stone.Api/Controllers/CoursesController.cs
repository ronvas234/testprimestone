﻿using AutoMapper;
using Common.DTOs;
using Common.Entities;
using Common.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Stone.Api.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stone.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CoursesController : ControllerBase
    {
        private readonly ICourseService _service;
        private readonly IMapper _mapper;
        private readonly ILoggerService _logger;

        public CoursesController(ICourseService service, IMapper mapper, ILoggerService logger)
        {
            _service = service;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var ienumerable = _service.GetAll();
            var dto = _mapper.Map<IEnumerable<CoursesResponse>>(ienumerable);
            var response = new ApiResponse<IEnumerable<CoursesResponse>>(dto);

            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var entity = await _service.GetCourse(id);
            var dto = _mapper.Map<CoursesResponse>(entity);
            var response = new ApiResponse<CoursesResponse>(dto);

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(CoursesRequest request)
        {
            _logger.LogInformation("Start in Courses->Post with {CoursesRequest}", request);
            var entity = _mapper.Map<Courses>(request);

            await _service.AddCourse(entity);

            var dto = _mapper.Map<CoursesResponse>(entity);
            var response = new ApiResponse<CoursesResponse>(dto);

            return Ok(response);
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, CoursesRequest request)
        {
            _logger.LogInformation("Start in Courses->Put with {Id} {CoursesRequest}",id, request);
            var entity = _mapper.Map<Courses>(request);
            entity.Id = id;
            var result = await _service.UpdateCourse(entity);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            _logger.LogInformation("Start in Courses->Delete with {Id}", id);
            var result = await _service.DelCourse(id);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }
    }
}
