﻿using AutoMapper;
using Common.DTOs;
using Common.Entities;
using Common.Exceptions;
using Common.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Stone.Api.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stone.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentService _service;
        private readonly IMapper _mapper;
        private readonly ILoggerService _logger;
        public StudentsController(IStudentService service, IMapper mapper, ILoggerService logger)
        {
            _service = service;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var ienumerable = _service.GetAll();
            var dto = _mapper.Map<IEnumerable<StudentsResponse>>(ienumerable);
            var response = new ApiResponse<IEnumerable<StudentsResponse>>(dto);

            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var entity = await _service.GetStudent(id);
            var dto = _mapper.Map<StudentsResponse>(entity);
            var response = new ApiResponse<StudentsResponse>(dto);

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(StudentsRequest request)
        {
            _logger.LogInformation("Start in Students->Post with {StudentsRequest}", request);
            if (request.StudentsAddressLine == null)
            {
                _logger.LogError("Error in Students->Post with {StudentsRequest}", request);
                throw new NotFoundException("You must enter a valid address");
            }
            if (request.StudentsAddressLine.Count == 0)
            {
                _logger.LogError("Error in Students->Post with {StudentsRequest}", request);
                throw new NotFoundException("You must enter a valid address");
            }

            var entity = _mapper.Map<Students>(request);

            await _service.AddStudent(entity);

            var dto = _mapper.Map<StudentsResponse>(entity);
            var response = new ApiResponse<StudentsResponse>(dto);

            return Ok(response);
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, StudentsRequest request)
        {
            _logger.LogInformation("Start in Students->Put with {Id} {StudentsRequest}",id, request);
            if (request.StudentsAddressLine == null)
            {
                _logger.LogError("Error in Students->Put with {Id} {StudentsRequest}", id, request);
                throw new NotFoundException("You must enter a valid address");
            }
            if (request.StudentsAddressLine.Count == 0)
            {
                _logger.LogError("Error in Student->Put with {Id} {StudentsRequest}", id, request);
                throw new NotFoundException("You must enter a valid address");
            }
            var entity = _mapper.Map<Students>(request);
            entity.Id = id;
            var result = await _service.UpdateStudent(entity);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            _logger.LogInformation("Start in Students->Delete with {Id} ", id);
            var result = await _service.DelStudent(id);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }
    }
}
