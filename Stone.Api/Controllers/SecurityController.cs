﻿using AutoMapper;
using AutoMapper.Configuration;
using Common.DTOs;
using Common.Entities;
using Common.Exceptions;
using Common.Interfaces.Services;
using Infraestructure.Options;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Stone.Api.Response;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Stone.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class SecurityController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly AuthenticationOptions _authenticationOptions;
        private readonly ISecurityService _securityService;
        private readonly IPasswordService _passwordService;
        private readonly IStudentService _studentService;
        public SecurityController(IConfiguration _configuration, IMapper mapper,
            IOptions<AuthenticationOptions> appSetting, ISecurityService securityService,
            IPasswordService passwordService, IStudentService studentService)
        {
            _mapper = mapper;
            _authenticationOptions = appSetting.Value;
            _securityService = securityService;
            _passwordService = passwordService;
            _studentService = studentService;
        }


        [HttpPost("Authentication")]
        public async Task<IActionResult> Authentication(UsersLoginRequest login)
        {
            //if it is a valid user
            var validation = await IsValidUser(login);
            var loginResponse = new LoginResponse();
            if (validation.Item1)
            {
                var student = _studentService.GetStudent(validation.Item2.StudentId);
                var studentDtos = _mapper.Map<StudentsResponse>(student);
                var token = GenerateToken(studentDtos);
                loginResponse.students = studentDtos;
                loginResponse.Token = token;
                var response = new ApiResponse<LoginResponse>(loginResponse);
                return Ok(response);
            }
            else
            {
                throw new NotFoundException("La contraseña no es valida");
            }
        }

        private async Task<(bool, Users)> IsValidUser(UsersLoginRequest login)
        {
            var user = await _securityService.GetLoginByCredentials(login);
            var isValid = _passwordService.Check(user.Password, login.Password);
            return (isValid, user);
        }

        private string GenerateToken(StudentsResponse users)
        {
            //Header
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authenticationOptions.SecretKey));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
            var header = new JwtHeader(signingCredentials);

            //Claims
            var claims = new[]
            {
                new Claim("FirtName", users.FirstName),
                new Claim("LastName", users.LastName),
                new Claim("Id", users.Id.ToString()),
            };

            //Payload
            var payload = new JwtPayload
            (
                _authenticationOptions.Issuer,
                _authenticationOptions.Audience,
                claims,
                DateTime.Now,
                DateTime.UtcNow.AddMinutes(60)
            );

            var token = new JwtSecurityToken(header, payload);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}
