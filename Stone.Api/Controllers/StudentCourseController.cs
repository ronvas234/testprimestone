﻿using AutoMapper;
using Common.DTOs;
using Common.Entities;
using Common.Interfaces.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Stone.Api.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Stone.Api.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class StudentCourseController : ControllerBase
    {
        private readonly IStudentCourseService _service;
        private readonly IMapper _mapper;
        private readonly ILoggerService _logger;

        public StudentCourseController(IStudentCourseService service, IMapper mapper, ILoggerService logger)
        {
            _service = service;
            _mapper = mapper;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var ienumerable = _service.GetAll();
            var dto = _mapper.Map<IEnumerable<StudentsCoursesResponse>>(ienumerable);
            var response = new ApiResponse<IEnumerable<StudentsCoursesResponse>>(dto);

            return Ok(response);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var entity = await _service.GetStudentCourse(id);
            var dto = _mapper.Map<StudentsResponse>(entity);
            var response = new ApiResponse<StudentsResponse>(dto);

            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> Post(StudentsCoursesRequest request)
        {
            _logger.LogInformation("Start in StudentCourse->Post with {StudentsCoursesRequest}", request);
            var entity = _mapper.Map<StudentsCourses>(request);

            await _service.AddStudentCourse(entity);

            var dto = _mapper.Map<StudentsCourses>(entity);
            var response = new ApiResponse<StudentsCourses>(dto);

            return Ok(response);
        }


        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, StudentsCoursesRequest request)
        {
            _logger.LogInformation("Start in StudentCoruse->Put with {Id} {StudentsCoursesRequest}", id,request);
            var entity = _mapper.Map<StudentsCourses>(request);
            entity.Id = id;
            var result = await _service.UpdateStudentCourse(entity);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            _logger.LogInformation("Start in StudentCoure->Delete with {id}", id);
            var result = await _service.DelStudentCourse(id);
            var response = new ApiResponse<bool>(result);

            return Ok(response);
        }
    }
}
